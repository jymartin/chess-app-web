import userIsLoggedGuard from '../../../src/router/userIsLoggedGuard'
import { HOME } from '../../../src/router/routerNames'
import firebase from 'firebase/app'
import 'firebase/auth'
jest.mock('firebase')

describe('userIsLoggedGuard should', () => {
  test('continue if user is logged in firebase', () => {
    const next = jest.fn()
    const firebaseUser = { currentUser: 'anyUser' }
    firebase.auth = jest.fn(() => firebaseUser)

    userIsLoggedGuard('', '', next)

    expect(next).toBeCalledWith()
  })

  test('go to home page if user is not logged', () => {
    const next = jest.fn()
    const firebaseUser = { currentUser: null }
    firebase.auth = jest.fn(() => firebaseUser)

    userIsLoggedGuard('', '', next)

    expect(next).toBeCalledWith({ name: HOME })
  })
})
