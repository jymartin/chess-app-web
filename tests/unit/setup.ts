import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(Vuetify)
