import actions from '../../../src/store/actions/userActions'
import { SET_USER, SIGN_OUT_USER, SIGN_IN_USER } from '../../../src/store/actions/actionTypes'
import firebase from 'firebase/app'
import 'firebase/auth'
import { RESET_USER, SET_USER_TOKEN } from '../../../src/store/mutations/mutationTypes'

jest.mock('firebase')
jest.mock('@/router/router')

describe('user actions should', () => {
  let commit

  beforeEach(() => {
    commit = jest.fn()
  })

  test('call mutation to set user', () => {
    const user = { id: '1234' }
    actions[SET_USER]({ commit }, user)
    expect(commit).toBeCalledWith(SET_USER, user)
  })

  describe('sign in user', () => {
    test('add user to state', () => {
      const user = { id: '1234' }
      const token = 'anyToken'

      actions[SIGN_IN_USER]({ commit }, { user, token })

      expect(commit).toBeCalledWith(SET_USER, user)
    })

    test('add user token to state', () => {
      const user = { id: '1234' }
      const token = 'anyToken'

      actions[SIGN_IN_USER]({ commit }, { user, token })

      expect(commit).toBeCalledWith(SET_USER_TOKEN, token)
    })
  })

  describe('sign out user', () => {
    test('sign out in firebase', () => {
      const authObject = {
        signOut: jest.fn(() => Promise.resolve())
      }
      firebase.auth = () => authObject

      actions[SIGN_OUT_USER]({ commit })

      expect(authObject.signOut).toBeCalled()
    })

    test('call mutation to reset user when it is signed out in firebase', async () => {
      const authObject = {
        signOut: jest.fn(() => Promise.resolve())
      }
      firebase.auth = () => authObject

      await actions[SIGN_OUT_USER]({ commit })

      expect(commit).toBeCalledWith(RESET_USER)
    })

    test('do not call mutation to reset user when it is not signed out in firebase', async () => {
      const authObject = {
        signOut: jest.fn(() => Promise.reject(new Error('something bad happened')))
      }
      firebase.auth = () => authObject

      await actions[SIGN_OUT_USER]({ commit })

      expect(commit).not.toBeCalledWith(RESET_USER)
    })
  })
})
