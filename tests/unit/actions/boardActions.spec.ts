import { SELECT_SQUARE, GET_BOARD } from '../../../src/store/actions/actionTypes'
import actions from '../../../src/store/actions/boardActions'
import { SET_SELECTED_SQUARE, SET_BOARD } from '../../../src/store/mutations/mutationTypes'
import { chessGameClient } from '../../../src/clients/clientFactory'

describe('BoardActions', () => {
  let commit: any

  beforeEach(() => {
    commit = jest.fn()
  })

  test('call to get board', async () => {
    const expectedBoard = {}
    chessGameClient.getBoard = jest.fn(() => Promise.resolve(expectedBoard))

    await actions[GET_BOARD]({ commit }, { id: 'gameId' })

    expect(commit).toHaveBeenCalledWith(SET_BOARD, expectedBoard)
  })

  describe('select square should', () => {
    beforeEach(() => {
      chessGameClient.movePiece = jest.fn()
    })

    it('set square as selected when there is no square selected', () => {
      const state = { selectedSquare: '' }
      const position = 'e4'
      const id = 'gameId'

      actions[SELECT_SQUARE]({ commit, state }, { position, id })

      expect(commit).toHaveBeenCalledWith(SET_SELECTED_SQUARE, position, id)
    })

    it('call backend to move piece when there is an square selected', () => {
      const state = { selectedSquare: 'e4' }
      const position = 'e5'
      const id = 'gameId'

      actions[SELECT_SQUARE]({ commit, state }, { position, id })

      expect(chessGameClient.movePiece).toHaveBeenCalledWith(state.selectedSquare, position, id)
      expect(commit).toHaveBeenCalledWith(SET_SELECTED_SQUARE, '')
    })
  })
})
