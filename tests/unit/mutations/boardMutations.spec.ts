import { SET_SELECTED_SQUARE, SET_BOARD } from '../../../src/store/mutations/mutationTypes'
import boardMutations from '../../../src/store/mutations/boardMutations'
import { RootState } from '@/storeTypes'

describe('Board mutations', () => {
  test('set selected square', () => {
    const state = { selectedSquare: '' } as RootState
    const position = 'e7'

    boardMutations[SET_SELECTED_SQUARE](state, position)

    expect(state.selectedSquare).toBe(position)
  })

  test('set board', () => {
    const state = { board: {} } as RootState
    const board = {}

    boardMutations[SET_BOARD](state, board)

    expect(state.board).toBe(board)
  })
})
