import userMutations from '../../../src/store/mutations/userMutations'
import { RESET_USER, SET_USER, RESET_USER_TOKEN, SET_USER_TOKEN } from '../../../src/store/mutations/mutationTypes'

describe('user mutations should', () => {
  test('set user', () => {
    const state = { user: {} }
    const user = { id: '88349' }

    userMutations[SET_USER](state, user)

    expect(state.user).toBe(user)
  })

  test('reset user', () => {
    const state = { user: { id: '88349' } }

    userMutations[RESET_USER](state)

    expect(state.user).toEqual({})
  })

  test('reset user token', () => {
    const state = { token: 'anyToken' }

    userMutations[RESET_USER_TOKEN](state)

    expect(state.token).toEqual('')
  })

  test('set user token', () => {
    const state = { token: '' }
    const token = 'theToken'

    userMutations[SET_USER_TOKEN](state, token)

    expect(state.token).toEqual(token)
  })
})
