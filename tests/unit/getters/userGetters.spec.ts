import userGetters from '../../../src/store/getters/userGetters'
import { USER_IS_LOGGED } from '../../../src/store/getters/getterTypes'

describe('user getters', () => {
  describe('user is logged should', () => {
    test('return false when user is not logged', () => {
      const state = { user: {} }
      const isLogged = userGetters[USER_IS_LOGGED](state)
      expect(isLogged).toBe(false)
    })

    test('return true when user is logged', () => {
      const state = { user: { name: 'any' } }
      const isLogged = userGetters[USER_IS_LOGGED](state)
      expect(isLogged).toBe(true)
    })
  })
})
