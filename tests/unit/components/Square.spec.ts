import Square from '../../../src/components/Square.vue'
import Piece from '../../../src/components/Piece.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'
import Vuex from 'vuex'
import { GET_PIECE_FOR } from '../../../src/store/getters/getterTypes'

describe('Square.vue', () => {
  function AnSquare () {
    let wrapper : Wrapper<any>
    let getPieceGetter: jest.Mock<Object | undefined>;
    getPieceGetter = jest.fn(() => undefined)
    const getters = {
      [GET_PIECE_FOR]: () => getPieceGetter
    }
    const store = new Vuex.Store({
      getters
    })
    const propsData = {
      color: 'black',
      rowNumber: 1,
      column: 'a'
    }

    function withColor (color: string) {
      propsData.color = color
      return self
    }

    function withColumn (column: string) {
      propsData.column = column
      return self
    }

    function withRow (row: number) {
      propsData.rowNumber = row
      return self
    }

    function withPiece (piece: object) {
      getPieceGetter.mockImplementation(() => piece)
      return self
    }

    function build () {
      wrapper = shallowMount(Square, {
        propsData,
        store
      })
      return self
    }

    const self = {
      withColor,
      withPiece,
      withColumn,
      withRow,
      build,
      getBackgroundColor: () => wrapper.find('div').attributes().class,
      getPiece: () => wrapper.find(Piece),
      emitted: () => wrapper.emitted(),
      whenClicked: () => wrapper.find('div').trigger('click'),
      getPieceGetter
    }

    return self
  }

  it('display a white square', () => {
    const square = AnSquare().withColor('white').build()

    expect(square.getBackgroundColor()).toMatch('white-square')
  })

  it('display a black square', () => {
    const square = AnSquare().withColor('black').build()

    expect(square.getBackgroundColor()).toMatch('black-square')
  })

  it('display piece', () => {
    const piece = {}
    const column = 'c'
    const row = 6
    const square = AnSquare()
      .withColumn(column)
      .withRow(row)
      .withPiece(piece)
      .build()

    const expectedPosition = 'c6'
    expect(square.getPiece().exists()).toBe(true)
    expect(square.getPiece().props().piece).toBe(piece)
    expect(square.getPieceGetter).toHaveBeenCalledWith(expectedPosition)
  })

  it('do not display any piece', () => {
    const column = 'c'
    const row = 6
    const square = AnSquare()
      .withColumn(column)
      .withRow(row)
      .build()

    const expectedPosition = 'c6'
    expect(square.getPiece().exists()).toBe(false)
    expect(square.getPieceGetter).toHaveBeenCalledWith(expectedPosition)
  })

  it('emit event when square is selected', () => {
    const column = 'c'
    const row = 6
    const square = AnSquare()
      .withColumn(column)
      .withRow(row)
      .build()

    square.whenClicked()

    const expectedPosition = 'c6'
    expect(square.emitted().selected).toEqual([[expectedPosition]])
  })
})
