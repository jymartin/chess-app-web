import NavigationDrawer from '@/components/Basic/NavigationDrawer.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'
import { VNavigationDrawer, VIcon, VListItemTitle, VListItem } from 'vuetify/lib'
import { FunctionalComponentOptions } from 'vue'

describe('NavigationDrawer.vue', () => {
  function ANavigationDrawer () {
    let wrapper : Wrapper<any>
    let propsData = {
      menuOptions: []
    }

    function build () {
      wrapper = shallowMount(NavigationDrawer, { propsData })
      return self
    }

    function withProps (props: object) {
      propsData = {
        ...propsData,
        ...props
      }
      return self
    }

    const self = {
      createComponent: build,
      withProps,
      hide: () => wrapper.setProps({ visible: false }),
      display: () => wrapper.setProps({ visible: true }),
      isDisplayed: () => wrapper.find(VNavigationDrawer as FunctionalComponentOptions).props().value,
      emitted: () => wrapper.emitted(),
      find: (component: any) => wrapper.find(component),
      findAll: (component: any) => wrapper.findAll(component),
      menuIconAt: (position: number) => wrapper.findAll(VIcon as FunctionalComponentOptions).at(position).text(),
      menuTitleAt: (position: number) => wrapper.findAll(VListItemTitle as FunctionalComponentOptions).at(position).text(),
      whenClickOnMenuOption: (position: number) => wrapper.findAll(VListItem as FunctionalComponentOptions).at(position).vm.$emit('click')
    }

    return self
  }

  it('display navigation drawer by default', () => {
    const navigationDrawer = ANavigationDrawer().createComponent()

    expect(navigationDrawer.find(VNavigationDrawer).exists()).toBe(true)
    expect(navigationDrawer.isDisplayed()).toBe(true)
  })

  it('hide navigation drawer', () => {
    const navigationDrawer = ANavigationDrawer()
      .withProps({ value: true })
      .createComponent()

    navigationDrawer.hide()

    expect(navigationDrawer.isDisplayed()).toBe(false)
  })

  it('display navigation drawer', () => {
    const navigationDrawer = ANavigationDrawer()
      .withProps({ value: false })
      .createComponent()

    navigationDrawer.display()

    expect(navigationDrawer.isDisplayed()).toBe(true)
  })

  it('display menu options', () => {
    const menuOptions = [
      { icon: 'account', title: 'Login' },
      { icon: 'information', title: 'About' }
    ]
    const navigationDrawer = ANavigationDrawer()
      .withProps({ menuOptions })
      .createComponent()

    expect(navigationDrawer.menuIconAt(0)).toBe(menuOptions[0].icon)
    expect(navigationDrawer.menuTitleAt(0)).toBe(menuOptions[0].title)
    expect(navigationDrawer.menuIconAt(1)).toBe(menuOptions[1].icon)
    expect(navigationDrawer.menuTitleAt(1)).toBe(menuOptions[1].title)
  })

  it('emit event when menu option clicked', () => {
    const menuOptions = [
      { icon: 'account', title: 'Login', eventName: 'LogInEvent' },
      { icon: 'information', title: 'About', eventName: 'AboutEvent' },
      { icon: 'game', title: 'Game', eventName: 'GameEvent' }
    ]
    const navigationDrawer = ANavigationDrawer()
      .withProps({ menuOptions })
      .createComponent()

    navigationDrawer.whenClickOnMenuOption(1)

    expect(navigationDrawer.emitted()[menuOptions[1].eventName]).toBeTruthy()
  })
})
