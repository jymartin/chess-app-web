import TopBar from '@/components/Basic/TopBar.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'
import { VAppBar, VAppBarNavIcon, VToolbarTitle } from 'vuetify/lib'
import { FunctionalComponentOptions } from 'vue'

describe('TopBar.vue', () => {
  function ATopBar () {
    let wrapper: Wrapper<any>

    function build () {
      wrapper = shallowMount(TopBar, {
        stubs: { VAppBarNavIcon, VToolbarTitle }
      })
      return self
    }

    const self = {
      createComponent: build,
      clickMenuButton: () => wrapper.find(VAppBarNavIcon as FunctionalComponentOptions).vm.$emit('click'),
      clickTitle: () => wrapper.find(VToolbarTitle as FunctionalComponentOptions).trigger('click'),
      emitted: () => wrapper.emitted(),
      find: (component: any) => wrapper.find(component)
    }

    return self
  }

  it('display top bar', () => {
    const topBar = ATopBar().createComponent()

    expect(topBar.find(VAppBar).exists()).toBe(true)
  })

  it('emit event when click on menu button', () => {
    const topBar = ATopBar().createComponent()

    topBar.clickMenuButton()

    expect(topBar.emitted().menuButtonClicked).toBeTruthy()
  })

  it('emit event when click on menu button', () => {
    const topBar = ATopBar().createComponent()

    topBar.clickTitle()

    expect(topBar.emitted().titleClicked).toBeTruthy()
  })
})
