import Board from '../../../src/components/Board.vue'
import Row from '../../../src/components/Row.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'
import { GET_BOARD, SELECT_SQUARE } from '../../../src/store/actions/actionTypes'
import { GET_USER_TOKEN } from '@/store/getters/getterTypes'

describe('Board.vue', () => {
  function ABoard () {
    let wrapper : Wrapper<any>
    const getters = {
      [GET_USER_TOKEN]: () => 'token'
    }
    const mockStore = { dispatch: jest.fn(), state: {}, getters }

    function build () {
      wrapper = shallowMount(Board, {
        mocks: { $store: mockStore }
      })
      return self
    }

    const self = {
      build,
      emitted: () => wrapper.emitted(),
      getRows: () => wrapper.findAll(Row),
      dispathAction: () => mockStore.dispatch,
      whenSquareSelected: (position: string) => wrapper.find(Row).vm.$emit('squareSelected', position)
    }

    return self
  }

  it('display eight rows', () => {
    const board = ABoard().build()

    const rows = board.getRows()
    expect(rows.at(0).props()).toMatchObject({ rowNumber: 8 })
    expect(rows.at(1).props()).toMatchObject({ rowNumber: 7 })
    expect(rows.at(2).props()).toMatchObject({ rowNumber: 6 })
    expect(rows.at(3).props()).toMatchObject({ rowNumber: 5 })
    expect(rows.at(4).props()).toMatchObject({ rowNumber: 4 })
    expect(rows.at(5).props()).toMatchObject({ rowNumber: 3 })
    expect(rows.at(6).props()).toMatchObject({ rowNumber: 2 })
    expect(rows.at(7).props()).toMatchObject({ rowNumber: 1 })
  })

  it('call action to get chess game', () => {
    const board = ABoard().build()

    expect(board.dispathAction()).toHaveBeenCalledWith(GET_BOARD)
  })

  it('call action when square has been selected', () => {
    const position = 'h4'
    const board = ABoard().build()

    board.whenSquareSelected(position)

    expect(board.dispathAction()).toHaveBeenCalledWith(SELECT_SQUARE, position)
  })
})
