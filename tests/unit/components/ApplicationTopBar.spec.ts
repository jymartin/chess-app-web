import ApplicationTopBar from '@/components/ApplicationTopBar.vue'
import NavigationDrawer from '@/components/Basic/NavigationDrawer.vue'
import TopBar from '@/components/Basic/TopBar.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'
import { USER_IS_LOGGED } from '../../../src/store/getters/getterTypes'
import Vuex from 'vuex'
import { SIGN_OUT_USER } from '../../../src/store/actions/actionTypes'
import { LOGIN, HOME, BOARD } from '../../../src/router/routerNames'

describe('ApplicationTopBar.vue', () => {
  function anApplicationTopBar () {
    let wrapper: Wrapper<any>
    let userIsLogged = false
    const getters = {
      [USER_IS_LOGGED]: () => userIsLogged
    }
    const actions = {
      [SIGN_OUT_USER]: jest.fn()
    }
    const store = new Vuex.Store({ getters, actions })
    const router = { push: jest.fn() }

    function build () {
      wrapper = shallowMount(ApplicationTopBar, {
        store,
        mocks: {
          $router: router
        }
      })
      return self
    }

    function withNotLoggedUser () {
      userIsLogged = false
      return self
    }

    function withLoggedUser () {
      userIsLogged = true
      return self
    }

    const self = {
      build,
      withNotLoggedUser,
      withLoggedUser,
      router,
      clickOnMenuButton: () => wrapper.find(TopBar).vm.$emit('menuButtonClicked'),
      whenClickOnTitle: () => wrapper.find(TopBar).vm.$emit('titleClicked'),
      find: (component: any) => wrapper.find(component),
      isNavigationDrawerDisplayed: () => wrapper.find(NavigationDrawer).props().visible,
      navigationDrawerOptions: () => wrapper.find(NavigationDrawer).props().menuOptions,
      onLogoutClicked: () => wrapper.find(NavigationDrawer).vm.$emit('logOut'),
      whenGameMenuOptionClicked: () => wrapper.find(NavigationDrawer).vm.$emit('game'),
      onLogin: () => wrapper.find(NavigationDrawer).vm.$emit('logIn'),
      logoutAction: () => actions[SIGN_OUT_USER]
    }

    return self
  }

  it('display topbar and navigation drawer', () => {
    const applicationTopBar = anApplicationTopBar().build()

    expect(applicationTopBar.find(TopBar).isVisible()).toBe(true)
    expect(applicationTopBar.find(NavigationDrawer).exists()).toBe(true)
  })

  it('show navigation drawer by default', () => {
    const applicationTopBar = anApplicationTopBar().build()

    expect(applicationTopBar.isNavigationDrawerDisplayed()).toBe(true)
  })

  it('hide navigation drawer when menu button is clicked when it is already displayed', () => {
    const applicationTopBar = anApplicationTopBar().build()

    applicationTopBar.clickOnMenuButton()

    expect(applicationTopBar.isNavigationDrawerDisplayed()).toBe(false)
  })

  it('show navigation drawer when menu button is clicked when it is hidden', () => {
    const applicationTopBar = anApplicationTopBar().build()
    applicationTopBar.clickOnMenuButton()

    applicationTopBar.clickOnMenuButton()

    expect(applicationTopBar.isNavigationDrawerDisplayed()).toBe(true)
  })

  it('go to home page when click on app title', () => {
    const applicationTopBar = anApplicationTopBar().build()

    applicationTopBar.whenClickOnTitle()

    expect(applicationTopBar.router.push).toBeCalledWith({ name: HOME })
  })

  describe('when user is not logged in', () => {
    it('display log in menu option', () => {
      const applicationTopBar = anApplicationTopBar()
        .withNotLoggedUser()
        .build()

      const expectedMenuOptions = [ { icon: 'mdi-account', title: 'Log in', eventName: 'logIn' } ]
      expect(applicationTopBar.navigationDrawerOptions()).toEqual(expectedMenuOptions)
    })

    it('go to login page when login option clicked', () => {
      const applicationTopBar = anApplicationTopBar()
        .withNotLoggedUser()
        .build()

      applicationTopBar.onLogin()

      expect(applicationTopBar.router.push).toBeCalledWith({ name: LOGIN })
    })
  })

  describe('when user is logged in', () => {
    it('display log out menu option', () => {
      const applicationTopBar = anApplicationTopBar()
        .withLoggedUser()
        .build()

      const expectedMenuOptions = [
        { icon: 'mdi-logout', title: 'Log out', eventName: 'logOut' },
        { icon: 'mdi-checkerboard', title: 'Game', eventName: 'game' }
      ]
      expect(applicationTopBar.navigationDrawerOptions()).toEqual(expectedMenuOptions)
    })

    it('execute sign out action when click on logout option', () => {
      const applicationTopBar = anApplicationTopBar()
        .withLoggedUser()
        .build()

      applicationTopBar.onLogoutClicked()

      expect(applicationTopBar.logoutAction()).toBeCalledTimes(1)
    })

    it('go to game page when click on game menu option', () => {
      const applicationTopBar = anApplicationTopBar()
        .withLoggedUser()
        .build()

      applicationTopBar.whenGameMenuOptionClicked()

      expect(applicationTopBar.router.push).toBeCalledWith({ name: BOARD })
    })
  })
})
