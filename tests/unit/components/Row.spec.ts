import Row from '../../../src/components/Row.vue'
import Square from '../../../src/components/Square.vue'
import { shallowMount, Wrapper } from '@vue/test-utils'

describe('Row.vue', () => {
  function ARow () {
    let wrapper : Wrapper<any>
    const propsData = { rowNumber: 1 }

    function withRow (row: number) {
      propsData.rowNumber = row
      return self
    }

    function build () {
      wrapper = shallowMount(Row, {
        propsData
      })
      return self
    }

    const self = {
      withRow,
      build,
      emitted: () => wrapper.emitted(),
      getSquares: () => wrapper.findAll(Square),
      whenSquareSelected: (position: string) => wrapper.find(Square).vm.$emit('selected', position)
    }

    return self
  }

  it('display eight squares', () => {
    const rowNumber = 4
    const row = ARow().withRow(rowNumber).build()

    const squares = row.getSquares()
    expect(squares.at(0).props()).toMatchObject({ rowNumber, column: 'a', color: 'white' })
    expect(squares.at(1).props()).toMatchObject({ rowNumber, column: 'b', color: 'black' })
    expect(squares.at(2).props()).toMatchObject({ rowNumber, column: 'c', color: 'white' })
    expect(squares.at(3).props()).toMatchObject({ rowNumber, column: 'd', color: 'black' })
    expect(squares.at(4).props()).toMatchObject({ rowNumber, column: 'e', color: 'white' })
    expect(squares.at(5).props()).toMatchObject({ rowNumber, column: 'f', color: 'black' })
    expect(squares.at(6).props()).toMatchObject({ rowNumber, column: 'g', color: 'white' })
    expect(squares.at(7).props()).toMatchObject({ rowNumber, column: 'h', color: 'black' })
  })

  it('display eight squares altering colors', () => {
    const rowNumber = 5
    const row = ARow().withRow(rowNumber).build()

    const squares = row.getSquares()
    expect(squares.at(0).props()).toMatchObject({ rowNumber, column: 'a', color: 'black' })
    expect(squares.at(1).props()).toMatchObject({ rowNumber, column: 'b', color: 'white' })
    expect(squares.at(2).props()).toMatchObject({ rowNumber, column: 'c', color: 'black' })
    expect(squares.at(3).props()).toMatchObject({ rowNumber, column: 'd', color: 'white' })
    expect(squares.at(4).props()).toMatchObject({ rowNumber, column: 'e', color: 'black' })
    expect(squares.at(5).props()).toMatchObject({ rowNumber, column: 'f', color: 'white' })
    expect(squares.at(6).props()).toMatchObject({ rowNumber, column: 'g', color: 'black' })
    expect(squares.at(7).props()).toMatchObject({ rowNumber, column: 'h', color: 'white' })
  })

  it('emit event when a square is selected', () => {
    const position = 'e5'
    const row = ARow().build()

    row.whenSquareSelected(position)

    expect(row.emitted().squareSelected).toEqual([[position]])
  })
})
