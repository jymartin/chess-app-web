export interface RootState {
  board: Board,
  selectedSquare: string,
  token: string,
  user: object
}

export type Board = Record<string, Piece>

export interface Piece {
  type: string,
  color: string
}