import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store'
import firebase from 'firebase/app'
import 'firebase/auth'
import { config } from './firebaseConfig'
import vuetify from './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { RESET_USER_TOKEN, RESET_USER } from './store/mutations/mutationTypes'
import '@mdi/font/css/materialdesignicons.css'
import { SIGN_IN_USER } from './store/actions/actionTypes'

Vue.config.productionTip = false
let vueApp: Vue

firebase.initializeApp(config)
firebase.auth().onAuthStateChanged((user) => {
  if (!vueApp) {
    startApplication(user)
    return
  }
  if (user) {
    user.getIdToken().then(token => {
      store.dispatch(SIGN_IN_USER, { user, token })
    })
  } else {
    store.commit(RESET_USER)
    store.commit(RESET_USER_TOKEN)
  }
})

async function startApplication (user: firebase.User | null) {
  if (user) {
    const token = await user.getIdToken()
    store.dispatch(SIGN_IN_USER, { user, token })
  }
  vueApp = runVueApp()
}

function runVueApp () {
  return new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount('#app')
}
