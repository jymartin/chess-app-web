export default function ChessGameClient (restClient: RestClient) {
  function getBoard () {
    return restClient.get('https://localhost:5001/api/games/4e6765f5-783c-4210-b171-8e5cc2a299ba')
  }

  function getGame (id: string) {
    return restClient.get(`https://localhost:5001/api/games/${id}`)
  }

  function movePiece (from: string, to: string, id: string) {
    restClient.post(`https://localhost:5001/api/games/${id}/movement/${from}/${to}`)
  }

  function createGame () {
    restClient.post(`https://localhost:5001/api/games`)
  }

  function getGameSummaries () {
    return restClient.get(`https://localhost:5001/api/games`)
  }

  return {
    getBoard,
    movePiece,
    createGame,
    getGameSummaries,
    getGame
  }
}
