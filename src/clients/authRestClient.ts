import axios from 'axios'
import store from '../store'

export default function AuthRestClient () : RestClient {
  function get (url: string) {
    return axios.get(url, { headers: { 'Authorization': `bearer ${store.state.token}` } })
      .then(response => response.data)
  }

  function post (url: string, body?: object) {
    return axios.post(url, body, { headers: { 'Authorization': `bearer ${store.state.token}` } })
      .then(response => response.data)
  }

  return {
    get,
    post
  }
}
