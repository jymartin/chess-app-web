import ChessGameClient from './chessGameClient'
import AuthRestClient from './authRestClient'

export const chessGameClient = ChessGameClient(AuthRestClient())
