interface RestClient {
  get (url: string) : any
  post (url: string, body?: object) : any
}