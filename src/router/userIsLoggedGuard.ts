import { NavigationGuard, Route, RawLocation } from 'vue-router'
import firebase from 'firebase/app'
import 'firebase/auth'
import { HOME } from './routerNames'

const userIsLoggedGuard: NavigationGuard = (to: Route, from: Route, next: (to?: RawLocation | void) => void) => {
  const firebaseUser = firebase.auth().currentUser
  if (firebaseUser !== null) {
    next()
    return
  }
  next({ name: HOME })
}

export default userIsLoggedGuard
