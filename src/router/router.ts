import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/views/Dashboard.vue'
import LoginPage from '@/views/LoginPage.vue'
import Board from '@/components/Board.vue'
import { HOME, BOARD, LOGIN, GAME } from './routerNames'
import userIsLoggedGuard from './userIsLoggedGuard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: HOME,
      component: Dashboard
    },
    {
      path: '/board',
      name: BOARD,
      component: Board,
      beforeEnter: userIsLoggedGuard
    },
    {
      path: '/game/:id',
      name: GAME,
      component: Board,
      beforeEnter: userIsLoggedGuard
    },
    {
      path: '/login',
      name: LOGIN,
      component: LoginPage
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
    }
  ]
})
