export default {
  Pawn: 'Pawn',
  Rock: 'Rock',
  Knight: 'Knight',
  Bishop: 'Bishop',
  Queen: 'Queen',
  King: 'King'
}
