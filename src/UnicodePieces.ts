import PieceNames from './PieceNames'
import BlackBishop from '@/assets/pieces/cburnett/bB.svg'
import BlackKing from '@/assets/pieces/cburnett/bK.svg'
import BlackKnight from '@/assets/pieces/cburnett/bN.svg'
import BlackPawn from '@/assets/pieces/cburnett/bP.svg'
import BlackQueen from '@/assets/pieces/cburnett/bQ.svg'
import BlackRock from '@/assets/pieces/cburnett/bR.svg'
import WhiteBishop from '@/assets/pieces/cburnett/wB.svg'
import WhiteKing from '@/assets/pieces/cburnett/wK.svg'
import WhiteKnight from '@/assets/pieces/cburnett/wN.svg'
import WhitePawn from '@/assets/pieces/cburnett/wP.svg'
import WhiteQueen from '@/assets/pieces/cburnett/wQ.svg'
import WhiteRock from '@/assets/pieces/cburnett/wR.svg'

export const BlackPieces = {
  [PieceNames.Pawn]: BlackPawn,
  [PieceNames.Knight]: BlackKnight,
  [PieceNames.Bishop]: BlackBishop,
  [PieceNames.Rock]: BlackRock,
  [PieceNames.Queen]: BlackQueen,
  [PieceNames.King]: BlackKing
}

export const WhitePieces = {
  [PieceNames.Pawn]: WhitePawn,
  [PieceNames.Knight]: WhiteKnight,
  [PieceNames.Bishop]: WhiteBishop,
  [PieceNames.Rock]: WhiteRock,
  [PieceNames.Queen]: WhiteQueen,
  [PieceNames.King]: WhiteKing
}

export const Pieces = {
  White: WhitePieces,
  Black: BlackPieces
}
