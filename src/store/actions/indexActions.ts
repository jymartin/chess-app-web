import BoardActions from './boardActions'
import userActions from './userActions'

export default {
  ...BoardActions,
  ...userActions
}
