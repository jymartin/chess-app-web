import { GET_BOARD, SELECT_SQUARE } from './actionTypes'
import { SET_BOARD, SET_SELECTED_SQUARE } from '../mutations/mutationTypes'
import { chessGameClient } from '../../clients/clientFactory'

export default {
  [GET_BOARD]: (context: any, { id }: { id: string }) => {
    console.log('id:' + id)
    chessGameClient.getGame(id)
      .then((response: any) => context.commit(SET_BOARD, response))
  },
  [SELECT_SQUARE]: (context: any, { position, id }: {position: string, id: string}) => {
    if (context.state.selectedSquare !== '') {
      chessGameClient.movePiece(context.state.selectedSquare, position, id)
      context.commit(SET_SELECTED_SQUARE, '')
    } else {
      context.commit(SET_SELECTED_SQUARE, position)
    }
  }
}
