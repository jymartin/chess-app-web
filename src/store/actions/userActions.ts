import { SET_USER, SIGN_OUT_USER, SIGN_IN_USER } from './actionTypes'
import firebase from 'firebase/app'
import 'firebase/auth'
import { RESET_USER, SET_USER_TOKEN } from '../mutations/mutationTypes'

export default {
  [SET_USER]: (context: any, user: object) => context.commit(SET_USER, user),
  [SIGN_OUT_USER]: async (context: any) => {
    try {
      await firebase.auth().signOut().then()
    } catch {
      return
    }
    context.commit(RESET_USER)
  },
  [SIGN_IN_USER]: (context: any, params: any) => {
    context.commit(SET_USER, params.user)
    context.commit(SET_USER_TOKEN, params.token)
  }
}
