import BoardMutations from './boardMutations'
import userMutations from './userMutations'

export default {
  ...BoardMutations,
  ...userMutations
}
