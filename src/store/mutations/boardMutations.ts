import { SET_BOARD, SET_SELECTED_SQUARE } from './mutationTypes'
import { RootState, Board } from '@/storeTypes'

export default {
  [SET_BOARD]: (state: RootState, newBoard: Board) => {
    state.board = newBoard
  },
  [SET_SELECTED_SQUARE]: (state: RootState, position: string) => {
    state.selectedSquare = position
  }
}
