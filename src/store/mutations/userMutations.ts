import { SET_USER_TOKEN, SET_USER, RESET_USER, RESET_USER_TOKEN } from './mutationTypes'

export default {
  [SET_USER_TOKEN]: (state: any, token: string) => { state.token = token },
  [RESET_USER_TOKEN]: (state: any) => { state.token = '' },
  [SET_USER]: (state: any, user: object) => { state.user = user },
  [RESET_USER]: (state: any) => { state.user = {} }
}
