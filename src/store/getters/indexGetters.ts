import boardGetters from './boardGetters'
import userGetters from './userGetters'

export default {
  ...boardGetters,
  ...userGetters
}
