import { GET_PIECE_FOR } from './getterTypes'

export default {
  [GET_PIECE_FOR]: (state: any) => (position: string) => {
    const piece = state.board[position]
    if (piece !== undefined) return piece
    return undefined
  }
}
