import { USER_IS_LOGGED, GET_USER_TOKEN } from './getterTypes'

export default {
  [USER_IS_LOGGED]: (state: any) => Object.keys(state.user).length !== 0,
  [GET_USER_TOKEN]: (state: any) => state.token
}
