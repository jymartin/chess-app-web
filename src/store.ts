import Vue from 'vue'
import Vuex from 'vuex'
import indexMutations from './store/mutations/indexMutations'
import indexActions from './store/actions/indexActions'
import indexGetters from './store/getters/indexGetters'
import { RootState } from './storeTypes'

Vue.use(Vuex)

export default new Vuex.Store<RootState>({
  state: {
    board: {},
    selectedSquare: '',
    token: '',
    user: {}
  },
  mutations: {
    ...indexMutations
  },
  actions: {
    ...indexActions
  },
  getters: {
    ...indexGetters
  }
})
