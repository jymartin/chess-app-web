module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    '@vue/standard',
    'plugin:vue/recommended',
    '@vue/typescript'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "vue/html-closing-bracket-newline": [
      "error", {
        "singleline": "never",
        "multiline": "never"
      }
    ]
  },
  parserOptions: {
    parser: '@typescript-eslint/parser'
  }
}
